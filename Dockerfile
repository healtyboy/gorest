FROM alpine:3.8
COPY rest /
RUN chmod 750 /rest
CMD ["/rest"]
