package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

var (
	addr = flag.String("addr", ":1323", "http service address")
	data map[string]string
)

func main() {
	flag.Parse()
	data = map[string]string{}
	router := httprouter.New()
	router.GET("/entry/:key", show)
	router.GET("/list", show)
	router.PUT("/entry/:key/:value", update)
	err := http.ListenAndServe(*addr, router)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func show(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	k := p.ByName("key")
	if k == "" {
		fmt.Fprintf(w, "Read list: %v", data)
		return
	}
	fmt.Fprintf(w, "Read entry: data[%s] = %s", k, data[k])
}

func update(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	k := p.ByName("key")
	v := p.ByName("value")
	data[k] = v
	fmt.Fprintf(w, "Updated: data[%s] = %s", k, data[k])
}
